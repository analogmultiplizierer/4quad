EESchema Schematic File Version 4
LIBS:M01-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L M01-rescue:MC1496D U1
U 1 1 5A764A3C
P 3800 4050
F 0 "U1" H 3500 3650 60  0000 C CNN
F 1 "MC1496D" H 3900 3650 60  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3900 4250 60  0001 C CNN
F 3 "" H 3900 4250 60  0000 C CNN
	1    3800 4050
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:MC1496D U2
U 1 1 5A764A7F
P 5900 4050
F 0 "U2" H 5600 3650 60  0000 C CNN
F 1 "MC1496D" H 6000 3650 60  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6000 4250 60  0001 C CNN
F 3 "" H 6000 4250 60  0000 C CNN
	1    5900 4050
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:R_Small-device Rg1
U 1 1 5A764B0C
P 2950 3850
F 0 "Rg1" V 2900 3650 50  0000 L CNN
F 1 "33k" V 2950 3800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2950 3850 50  0001 C CNN
F 3 "" H 2950 3850 50  0000 C CNN
	1    2950 3850
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device Rs1
U 1 1 5A764B47
P 2950 3750
F 0 "Rs1" V 2900 3550 50  0000 L CNN
F 1 "100" V 2950 3700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2950 3750 50  0001 C CNN
F 3 "" H 2950 3750 50  0000 C CNN
	1    2950 3750
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device Rs2
U 1 1 5A764B66
P 2950 4050
F 0 "Rs2" V 2900 3850 50  0000 L CNN
F 1 "100" V 2950 4000 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2950 4050 50  0001 C CNN
F 3 "" H 2950 4050 50  0000 C CNN
	1    2950 4050
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device Rb1
U 1 1 5A764B87
P 2950 4150
F 0 "Rb1" V 2900 3950 50  0000 L CNN
F 1 "10k" V 2950 4100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2950 4150 50  0001 C CNN
F 3 "" H 2950 4150 50  0000 C CNN
	1    2950 4150
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device R4
U 1 1 5A764BB0
P 4400 4250
F 0 "R4" H 4450 4300 50  0000 L CNN
F 1 "10k" H 4450 4200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 4250 50  0001 C CNN
F 3 "" H 4400 4250 50  0000 C CNN
	1    4400 4250
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:R_Small-device Rs3
U 1 1 5A764BD3
P 5150 3750
F 0 "Rs3" V 5100 3550 50  0000 L CNN
F 1 "100" V 5150 3700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 3750 50  0001 C CNN
F 3 "" H 5150 3750 50  0000 C CNN
	1    5150 3750
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device Rg2
U 1 1 5A764BFA
P 5150 3850
F 0 "Rg2" V 5100 3650 50  0000 L CNN
F 1 "33k" V 5150 3800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 3850 50  0001 C CNN
F 3 "" H 5150 3850 50  0000 C CNN
	1    5150 3850
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device Rs4
U 1 1 5A764C23
P 5150 4050
F 0 "Rs4" V 5100 3850 50  0000 L CNN
F 1 "100" V 5150 4000 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 4050 50  0001 C CNN
F 3 "" H 5150 4050 50  0000 C CNN
	1    5150 4050
	0    1    1    0   
$EndComp
$Comp
L M01-rescue:R_Small-device Rb2
U 1 1 5A764C4E
P 5150 4150
F 0 "Rb2" V 5100 3950 50  0000 L CNN
F 1 "10k" V 5150 4100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 4150 50  0001 C CNN
F 3 "" H 5150 4150 50  0000 C CNN
	1    5150 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 4150 4300 4150
Wire Wire Line
	4300 4350 4400 4350
Wire Wire Line
	3250 4250 3150 4250
Wire Wire Line
	3150 3550 3150 4250
Wire Wire Line
	3150 4500 6500 4500
Wire Wire Line
	6500 4500 6500 4350
Wire Wire Line
	6500 4350 6400 4350
Wire Wire Line
	6400 4150 6600 4150
Wire Wire Line
	6600 4150 6600 4600
Wire Wire Line
	6600 4600 4600 4600
Wire Wire Line
	4600 4600 4600 3950
Wire Wire Line
	4600 3950 4300 3950
Wire Wire Line
	5350 4150 5250 4150
Wire Wire Line
	5250 4050 5350 4050
Wire Wire Line
	5350 3850 5250 3850
Wire Wire Line
	5250 3750 5350 3750
Wire Wire Line
	5350 3950 4950 3950
Wire Wire Line
	4950 3950 4950 3850
Wire Wire Line
	4950 3850 5050 3850
Wire Wire Line
	3250 3850 3050 3850
Wire Wire Line
	3250 3950 2750 3950
Wire Wire Line
	2750 3950 2750 3850
Wire Wire Line
	2750 3850 2850 3850
Wire Wire Line
	3050 4050 3250 4050
Wire Wire Line
	3250 4150 3050 4150
Wire Wire Line
	3050 3750 3250 3750
Wire Wire Line
	2850 3750 2800 3750
Wire Wire Line
	2850 4050 2800 4050
Text GLabel 2800 3750 0    60   Input ~ 0
X2
Text GLabel 2800 4050 0    60   Input ~ 0
X1
Wire Wire Line
	4950 3750 5050 3750
Wire Wire Line
	5050 4050 4950 4050
Text GLabel 4950 3750 0    60   Input ~ 0
Y2
Text GLabel 4950 4050 0    60   Input ~ 0
Y1
Wire Wire Line
	4700 4250 5350 4250
Wire Wire Line
	4700 3300 4700 3550
Wire Wire Line
	6600 3950 6400 3950
$Comp
L M01-rescue:R_Small-device R6
U 1 1 5A76598B
P 4700 3200
F 0 "R6" H 4730 3220 50  0000 L CNN
F 1 "2k2" H 4730 3160 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4700 3200 50  0001 C CNN
F 3 "" H 4700 3200 50  0000 C CNN
	1    4700 3200
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:R_Small-device R7
U 1 1 5A7659F7
P 6600 3200
F 0 "R7" H 6630 3220 50  0000 L CNN
F 1 "2k2" H 6630 3160 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6600 3200 50  0001 C CNN
F 3 "" H 6600 3200 50  0000 C CNN
	1    6600 3200
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:R_Small-device R5
U 1 1 5A765A68
P 4400 4700
F 0 "R5" H 4450 4750 50  0000 L CNN
F 1 "33k" H 4450 4650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 4700 50  0001 C CNN
F 3 "" H 4400 4700 50  0000 C CNN
	1    4400 4700
	1    0    0    -1  
$EndComp
Connection ~ 4400 4350
Wire Wire Line
	5050 4150 4950 4150
$Comp
L M01-rescue:D-device D1
U 1 1 5A765FC4
P 4400 5050
F 0 "D1" H 4400 5150 50  0000 C CNN
F 1 "1N4148" H 4400 4950 50  0000 C CNN
F 2 "Diode_SMD:D_MiniMELF" H 4400 5050 50  0001 C CNN
F 3 "" H 4400 5050 50  0000 C CNN
	1    4400 5050
	0    -1   -1   0   
$EndComp
$Comp
L power:VSS #PWR02
U 1 1 5A76695F
P 6400 3750
F 0 "#PWR02" H 6400 3600 50  0001 C CNN
F 1 "VSS" H 6400 3900 50  0000 C CNN
F 2 "" H 6400 3750 50  0000 C CNN
F 3 "" H 6400 3750 50  0000 C CNN
	1    6400 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR03
U 1 1 5A766A43
P 4300 3750
F 0 "#PWR03" H 4300 3600 50  0001 C CNN
F 1 "VSS" H 4300 3900 50  0000 C CNN
F 2 "" H 4300 3750 50  0000 C CNN
F 3 "" H 4300 3750 50  0000 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR05
U 1 1 5A766B39
P 4400 5500
F 0 "#PWR05" H 4400 5350 50  0001 C CNN
F 1 "VSS" H 4400 5650 50  0000 C CNN
F 2 "" H 4400 5500 50  0000 C CNN
F 3 "" H 4400 5500 50  0000 C CNN
	1    4400 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3550 3950 3550
Wire Wire Line
	3950 3550 3950 3350
Connection ~ 4600 3950
Wire Wire Line
	3150 3550 3800 3550
Wire Wire Line
	3800 3550 3800 3350
Connection ~ 3150 4250
Wire Wire Line
	3500 3150 3450 3150
Wire Wire Line
	4250 3150 4300 3150
Wire Wire Line
	3450 2850 3800 2850
Wire Wire Line
	3450 2750 3450 2850
Wire Wire Line
	3800 2850 3800 2950
Connection ~ 3800 2850
Wire Wire Line
	3950 2850 3950 2950
Connection ~ 3950 2850
Connection ~ 4300 2850
Connection ~ 3450 2850
$Comp
L M01-rescue:D-device D5
U 1 1 5A7722F1
P 3450 2600
F 0 "D5" H 3450 2700 50  0000 C CNN
F 1 "1N4148" H 3450 2500 50  0000 C CNN
F 2 "Diode_SMD:D_MiniMELF" H 3450 2600 50  0001 C CNN
F 3 "" H 3450 2600 50  0001 C CNN
	1    3450 2600
	0    -1   -1   0   
$EndComp
$Comp
L M01-rescue:D-device D4
U 1 1 5A772558
P 3450 2300
F 0 "D4" H 3450 2400 50  0000 C CNN
F 1 "1N4148" H 3450 2200 50  0000 C CNN
F 2 "Diode_SMD:D_MiniMELF" H 3450 2300 50  0001 C CNN
F 3 "" H 3450 2300 50  0001 C CNN
	1    3450 2300
	0    -1   -1   0   
$EndComp
$Comp
L M01-rescue:D-device D3
U 1 1 5A772911
P 3450 2000
F 0 "D3" H 3450 2100 50  0000 C CNN
F 1 "1N4148" H 3450 1900 50  0000 C CNN
F 2 "Diode_SMD:D_MiniMELF" H 3450 2000 50  0001 C CNN
F 3 "" H 3450 2000 50  0001 C CNN
	1    3450 2000
	0    -1   -1   0   
$EndComp
$Comp
L M01-rescue:D-device D2
U 1 1 5A77297B
P 3450 1700
F 0 "D2" H 3450 1800 50  0000 C CNN
F 1 "1N4148" H 3450 1600 50  0000 C CNN
F 2 "Diode_SMD:D_MiniMELF" H 3450 1700 50  0001 C CNN
F 3 "" H 3450 1700 50  0001 C CNN
	1    3450 1700
	0    -1   -1   0   
$EndComp
$Comp
L power:VDD #PWR06
U 1 1 5A772DB4
P 3450 1550
F 0 "#PWR06" H 3450 1400 50  0001 C CNN
F 1 "VDD" H 3450 1700 50  0000 C CNN
F 2 "" H 3450 1550 50  0001 C CNN
F 3 "" H 3450 1550 50  0001 C CNN
	1    3450 1550
	1    0    0    -1  
$EndComp
Text Label 4950 4150 2    60   ~ 0
bias
Text Label 2450 4150 2    60   ~ 0
bias
Connection ~ 4700 3550
Wire Wire Line
	4400 4350 4400 4600
Wire Wire Line
	4400 3100 4400 4150
Wire Wire Line
	4400 2850 4400 2900
$Comp
L M01-rescue:R_Small-device R16
U 1 1 5A7788EB
P 4400 3000
F 0 "R16" H 4430 3020 50  0000 L CNN
F 1 "10k" H 4430 2960 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 3000 50  0001 C CNN
F 3 "" H 4400 3000 50  0000 C CNN
	1    4400 3000
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR07
U 1 1 5A779ACA
P 4700 3100
F 0 "#PWR07" H 4700 2950 50  0001 C CNN
F 1 "VDD" H 4700 3250 50  0000 C CNN
F 2 "" H 4700 3100 50  0001 C CNN
F 3 "" H 4700 3100 50  0001 C CNN
	1    4700 3100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR08
U 1 1 5A779B29
P 6600 3100
F 0 "#PWR08" H 6600 2950 50  0001 C CNN
F 1 "VDD" H 6600 3250 50  0000 C CNN
F 2 "" H 6600 3100 50  0001 C CNN
F 3 "" H 6600 3100 50  0001 C CNN
	1    6600 3100
	1    0    0    -1  
$EndComp
Text GLabel 6750 4050 0    60   Input ~ 0
Z
Text GLabel 8700 3450 2    60   Input ~ 0
W
$Comp
L power:VSS #PWR017
U 1 1 5A78CDBC
P 6150 5250
F 0 "#PWR017" H 6150 5100 50  0001 C CNN
F 1 "VSS" H 6150 5400 50  0000 C CNN
F 2 "" H 6150 5250 50  0000 C CNN
F 3 "" H 6150 5250 50  0000 C CNN
	1    6150 5250
	-1   0    0    1   
$EndComp
$Comp
L power:VDD #PWR018
U 1 1 5A78CE3F
P 6150 4850
F 0 "#PWR018" H 6150 4700 50  0001 C CNN
F 1 "VDD" H 6150 5000 50  0000 C CNN
F 2 "" H 6150 4850 50  0001 C CNN
F 3 "" H 6150 4850 50  0001 C CNN
	1    6150 4850
	1    0    0    -1  
$EndComp
Text GLabel 6100 5000 2    60   Input ~ 0
W
Text GLabel 6100 5100 2    60   Input ~ 0
Z
Text GLabel 2800 5000 2    60   Input ~ 0
X2
Text GLabel 2800 4900 2    60   Input ~ 0
X1
Text GLabel 2800 5200 2    60   Input ~ 0
Y2
Text GLabel 2800 5100 2    60   Input ~ 0
Y1
$Comp
L M01-rescue:BC847BS-transistors Q1
U 1 1 5A78F37A
P 4050 3150
F 0 "Q1" H 4250 3200 50  0000 L CNN
F 1 "NST45011MW6T1G" H 4250 3100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 4250 3250 50  0001 C CNN
F 3 "" H 4050 3150 50  0001 C CNN
	1    4050 3150
	-1   0    0    -1  
$EndComp
$Comp
L M01-rescue:BC847BS-transistors Q1
U 2 1 5A78F434
P 3700 3150
F 0 "Q1" H 3900 3200 50  0000 L CNN
F 1 "NST45011MW6T1G" H 3900 3100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 3900 3250 50  0001 C CNN
F 3 "" H 3700 3150 50  0001 C CNN
	2    3700 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3950 4600 3550
Wire Wire Line
	3150 4250 3150 4500
Wire Wire Line
	3800 2850 3950 2850
Wire Wire Line
	3950 2850 4300 2850
Wire Wire Line
	4300 2850 4400 2850
Wire Wire Line
	4700 3550 4700 4250
$Comp
L Reference_Voltage:TL431DBZ U3
U 1 1 5C21E762
P 4400 5400
F 0 "U3" V 4446 5330 50  0000 R CNN
F 1 "AN431AN-ATRG1" V 4355 5330 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4400 5250 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tl431.pdf" H 4400 5400 50  0001 C CIN
	1    4400 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 5200 4400 5250
Wire Wire Line
	4400 5250 4250 5250
Wire Wire Line
	4250 5250 4250 5400
Wire Wire Line
	4250 5400 4300 5400
Connection ~ 4400 5250
Wire Wire Line
	4400 5250 4400 5300
Wire Wire Line
	4300 2850 4300 3150
Wire Wire Line
	3450 2850 3450 3150
Connection ~ 4400 4150
Wire Wire Line
	4400 4800 4400 4850
Wire Wire Line
	4350 4850 4400 4850
Connection ~ 4400 4850
Wire Wire Line
	4400 4850 4400 4900
Text Label 4350 4850 2    60   ~ 0
bias
Wire Wire Line
	2450 4150 2850 4150
$Comp
L M01-rescue:Conn_01x04_Male-conn J1
U 1 1 5C2431A7
P 2600 5100
F 0 "J1" H 3050 5300 50  0000 C CNN
F 1 "2.54mm header" H 3300 5200 50  0000 C CNN
F 2 "Connector_Harwin:Harwin_M20-89004xx_1x04_P2.54mm_Horizontal" H 2600 5100 50  0001 C CNN
F 3 "~" H 2600 5100 50  0001 C CNN
	1    2600 5100
	1    0    0    1   
$EndComp
$Comp
L M01-rescue:Conn_01x04_Male-conn J2
U 1 1 5C24414F
P 5900 5000
F 0 "J2" H 6300 5250 50  0000 C CNN
F 1 "2.54mm header" H 6550 5150 50  0000 C CNN
F 2 "Connector_Harwin:Harwin_M20-89004xx_1x04_P2.54mm_Horizontal" H 5900 5000 50  0001 C CNN
F 3 "~" H 5900 5000 50  0001 C CNN
	1    5900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4900 6150 4850
Wire Wire Line
	6150 5200 6150 5250
Wire Wire Line
	6150 5200 6100 5200
Wire Wire Line
	6100 4900 6150 4900
$Comp
L M01-rescue:BC847BS-transistors Q2
U 1 1 5C2778E0
P 6950 4050
F 0 "Q2" H 7140 4096 50  0000 L CNN
F 1 "BC847BS" H 7140 4005 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 7150 4150 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/BC847BS.pdf" H 6950 4050 50  0001 C CNN
	1    6950 4050
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:BC847BS-transistors Q2
U 2 1 5C2784EC
P 8050 4050
F 0 "Q2" H 8241 4096 50  0000 L CNN
F 1 "BC847BS" H 8241 4005 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 8250 4150 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/BC847BS.pdf" H 8050 4050 50  0001 C CNN
	2    8050 4050
	-1   0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL082 U4
U 2 1 5C27ED60
P 8300 3450
F 0 "U4" H 8350 3750 50  0000 C CNN
F 1 "TL082" H 8400 3650 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8300 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 8300 3450 50  0001 C CNN
	2    8300 3450
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL082 U4
U 3 1 5C27FF96
P 8300 3450
F 0 "U4" H 8258 3496 50  0000 L CNN
F 1 "TL082" H 8258 3405 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8300 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 8300 3450 50  0001 C CNN
	3    8300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3550 7050 3550
Wire Wire Line
	7050 3850 7050 3550
Connection ~ 7050 3550
Wire Wire Line
	7050 3550 8000 3550
Wire Wire Line
	7950 3350 8000 3350
Wire Wire Line
	8700 3450 8650 3450
Wire Wire Line
	8650 3450 8650 4050
Wire Wire Line
	8650 4050 8250 4050
Connection ~ 8650 3450
Wire Wire Line
	8650 3450 8600 3450
$Comp
L M01-rescue:R_Small-device Re1
U 1 1 5C2AFC8A
P 7050 4350
F 0 "Re1" H 7080 4370 50  0000 L CNN
F 1 "10k" H 7080 4310 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7050 4350 50  0001 C CNN
F 3 "" H 7050 4350 50  0000 C CNN
	1    7050 4350
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:R_Small-device Re2
U 1 1 5C2B06A0
P 7950 4350
F 0 "Re2" H 7980 4370 50  0000 L CNN
F 1 "10k" H 7980 4310 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7950 4350 50  0001 C CNN
F 3 "" H 7950 4350 50  0000 C CNN
	1    7950 4350
	1    0    0    -1  
$EndComp
$Comp
L M01-rescue:R_Small-device Ree1
U 1 1 5C2B0A60
P 7500 4650
F 0 "Ree1" H 7530 4670 50  0000 L CNN
F 1 "10k" H 7530 4610 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7500 4650 50  0001 C CNN
F 3 "" H 7500 4650 50  0000 C CNN
	1    7500 4650
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR0101
U 1 1 5C2B1055
P 7500 4750
F 0 "#PWR0101" H 7500 4600 50  0001 C CNN
F 1 "VSS" H 7500 4900 50  0000 C CNN
F 2 "" H 7500 4750 50  0000 C CNN
F 3 "" H 7500 4750 50  0000 C CNN
	1    7500 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	7050 4450 7050 4500
Wire Wire Line
	7050 4500 7500 4500
Wire Wire Line
	7950 4500 7950 4450
Wire Wire Line
	7500 4550 7500 4500
Connection ~ 7500 4500
Wire Wire Line
	7500 4500 7950 4500
$Comp
L power:VDD #PWR0102
U 1 1 5C2D0602
P 8200 3150
F 0 "#PWR0102" H 8200 3000 50  0001 C CNN
F 1 "VDD" H 8200 3300 50  0000 C CNN
F 2 "" H 8200 3150 50  0001 C CNN
F 3 "" H 8200 3150 50  0001 C CNN
	1    8200 3150
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR0103
U 1 1 5C2D0FC4
P 8200 3750
F 0 "#PWR0103" H 8200 3600 50  0001 C CNN
F 1 "VSS" H 8200 3900 50  0000 C CNN
F 2 "" H 8200 3750 50  0000 C CNN
F 3 "" H 8200 3750 50  0000 C CNN
	1    8200 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 3300 6600 3350
$Comp
L M01-rescue:R_Small-device Rz1
U 1 1 5C2D1EFE
P 7350 3350
F 0 "Rz1" V 7450 3350 50  0000 C CNN
F 1 "0" V 7350 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7350 3350 50  0001 C CNN
F 3 "" H 7350 3350 50  0000 C CNN
	1    7350 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 3850 7950 3350
Wire Wire Line
	7450 3350 7950 3350
Connection ~ 7950 3350
Wire Wire Line
	7250 3350 6600 3350
Connection ~ 6600 3350
Wire Wire Line
	6600 3350 6600 3950
$EndSCHEMATC
