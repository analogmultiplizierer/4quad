# 4quad
a 4 quadrant analog Multiplier with differential multiplication inputs, summing input and single ended output

The design aims to provide a low-budget alternative to the AD633 and similar commercial operational amplifier at the cost of reduced specs and larger physical size

The circuit was modeled in LTspice and successfully built using the component values provided by the schematic file

The Board Layout has one signal layer only to aid etching/milling.
The rear layer is used for negative supply voltage exclusively, only a few connections need to be made

![](./3D.png)

## specs

The circuit has not extensively been tested, but apears to match the simulation very well.
The input common mode range is limited, apparently phase reversal occurs with several volts of positive common mode or very large input signals.
The circuit operates well at 100 kHz input frequency. The simulation results in ~230kHz -3dB frequency at 1V and 1Vs inputs.

The circuit has been tested at +/-15 Volts, drawing ~40 mA
